# Circuito Multipropósito PIC16F887: Voltímetro, Reloj y Mecanismo de Seguridad

Este repositorio contiene el código y los esquemas para un circuito electrónico versátil construido para el curso de Electrónica Digital.

## Resumen

Experimenta un proyecto integral que utiliza el microcontrolador PIC 16F887, integrando interrupciones, temporizadores, un teclado de matriz, convertidor analógico a digital (ADC) y comunicación serial. El proyecto cuenta con un voltímetro, reloj y un mecanismo de seguridad, mostrando una culminación de principios de electrónica digital.

## Esquemático
![Diagrama del Circuito](img/circuit.png)

### Características

- **Visualización del Reloj:** Configura y muestra la hora en formato de 24 horas utilizando el teclado de matriz.
- **Lecturas de Voltaje y Corriente:** Visualización en tiempo real de voltaje (0-5V) y corriente (0-5A) en cuatro displays de siete segmentos.
- **Modos Operativos:** Cuatro modos mostrados a través del Puerto E en código binario.
- **Multiplexación:** Utiliza un bus de datos de 7 bits para mostrar información en los cuatro segmentos.
- **Comunicación Serial:** Transmite valores de voltaje/corriente a una PC para su acceso a datos.

### Mecanismo de Seguridad

Un mecanismo contra cortocircuitos protege los circuitos del usuario al activar un relé cuando el voltaje se acerca a cero, desconectando la fuente de alimentación para evitar daños.

## Detalles Técnicos

Este proyecto emplea una variedad de componentes, incluyendo PIC16F887, potenciómetros, transistores, displays de siete segmentos, resistores, capacitores, LEDs, un relé, teclado de matriz y conectores. El archivo README aquí contiene información técnica detallada, incluyendo cálculos, utilización de componentes, especificaciones de polarización, velocidad de baudios, cálculos de tiempo de muestreo del ADC y una conclusión reflexiva sobre el éxito del proyecto.

## Comenzando

Para clonar este repositorio y explorar el proyecto:

```bash
git clone https://gitlab.com/english6060242/embedded-systems/pic/asm/01-voltimeter_and_clock.git
```

## Uso
Archivo Hex Preparado: Utiliza el archivo .hex proporcionado en la carpeta bin.

o

Compilar el Código: Compílalo tú mismo usando MPLAB u otro software similar. Una vez compilado, agrega el archivo hex generado a tu simulación o úsalo para programar tu PIC si el circuito está construido físicamente.

Nota: En el caso de implementación física, asegúrate de integrar un diodo en el relé, como se ilustra en la imagen del circuito. Este diodo es crucial para evitar posibles daños causados por el inductor.

## Licencia 
Este proyecto está licenciado bajo la [Licencia MIT](LICENSE.md).