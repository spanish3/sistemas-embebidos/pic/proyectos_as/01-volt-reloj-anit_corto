#include "p16f887.inc"

; CONFIG1
; __config 0xFFE1
 __CONFIG _CONFIG1, _FOSC_INTRC_NOCLKOUT & _WDTE_OFF & _PWRTE_ON & _MCLRE_ON & _CP_OFF & _CPD_OFF & _BOREN_OFF & _IESO_OFF & _FCMEN_OFF & _LVP_OFF
; CONFIG2
; __config 0xFFFF
 __CONFIG _CONFIG2, _BOR4V_BOR40V & _WRT_OFF

	ORG	0X00
	GOTO	START
 
	ORG	0X04
	GOTO	INTER
			;   Declaración de variables
CBLOCK	0X20
	MODO		;   Registro que contiene el modo de funcionamiento. Se se visualizarán distintos datos en el display de acuerdo a este.
	UNIDAD		;   El valor leido por el ADC se divide en unidad, décima y centésima para 
	DECENA		;   Poder visualizarlo en los Display. Se los llama unidad, decena y centena 
	CENTENA		;   por comodidad. Esto se guarda en estos tres registros.
	SELDISP		;   Este registro contiene el valor para habilitar/deshabilitar displays según corresponda.
	GW		;   Registro para guardar W cuando se produce una interrupción.
	GSTAT		;   Registro para guardar STATUS cuando se produce una interrupción.
	AUX		;   Varibale auxiliar para
	AUX2		;   Varibale auxiliar para
	AUX3		;   Varibale auxiliar para
	CONT		;   Contador para enviar 1 logicos a la fila correspondiente del teclado  matricial
	CONT2		;   Contador para que se incremente CONT cada 3 interrupciones por TMR0
	CONT3		;   Contador para generar 1 segundo, a partir de un TMR0 de 5ms, por eso se carga en 200
	CMOD		;   Contador analogo al Modo
	MCONT		;   Contador para  seleccionar que caracter enviar por puerto Serie
	ECONT		;   Contador para elegir que caracter enviar por puerto serie
	CONTCS		;   Contador para enviar por puerto serie cada 3 interrupciones por timer0
	NUM1		
	NUM2
	NUM3		;   Estos son los numeros a enviar por Display que contiene el valor decimal de la lectura del ADC
	NUMD		;   Valor a mostrar en un instante determinado por PORTD (Dato de los Display)
	SEG		;   Segundos, cuando llegan a 60, se incrementa 1 minuto
	UMIN		;   Unidad de los Minutos
	DMIN		;   Decena de los Minutos
	UHORA		;   Unidad de las Horas
	DHORA		;   Decena de las Horas
	SUMIN		;   Registro para setear Unidad de los Minutos
	SDMIN		;   Registro para setear Decena de los Minutos
	SUHORA		;   Registro para setear Unidad de los Horas
	SDHORA		;   Registro para setear Decena de los Horas
	CHARU		;   Caracter para enviar la unidad del valor leido por el ADC
	CHARD		;   Caracter para enviar la decima del valor leido por el ADC
	CHARC		;   Caracter para enviar la centesima del valor leido por el ADC
	CHAR0		;   Caracter a enviarpor puerto serie.
	ENDC		    ;	Fin de declaración de Variables

START	CLRF    NUMD	    ;Inicialización de Variables
	CLRF	SUMIN
	CLRF	SDMIN
	CLRF	SUHORA
	CLRF	SELDISP
	CLRF	CONT
	CLRF	GW
	CLRF	GSTAT
	CLRF	CONT2
	CLRF	MODO
	CLRF	AUX
	CLRF	CONT3
	CLRF	UMIN
	CLRF	DMIN
	MOVWF	UHORA
	MOVWF	DHORA
	MOVLW	b'10100000'	;   Interrupciones Habilitadas: Timer 0.
	MOVWF	INTCON
	BANKSEL	ADCON0		;   Configuración de ADC: Clock de Conversión: Fosc/32 ; 
	MOVLW	b'11000101'	;   Canal 1; ADON=1 -> Conversión Habilitada
	MOVWF	ADCON0		;
	BSF	STATUS,RP0
	MOVLW	b'11000110'	;   Configuración de OPTION_REG: Resistencias de pull up deshabilitadas.
	MOVWF	OPTION_REG	;   Prescaler = 128, asignado al TMR0; Fuente de clock del TMR0 = Fosc/4
	MOVLW	b'01110000'
	MOVWF	TRISB		;   Configuración Entrada - Salida
	MOVLW	0X03
	MOVWF	TRISA
	CLRF	TRISE
	CLRF	TRISC
	BSF	TRISC,7
	CLRF	TRISD
	BANKSEL	ADCON1		;   Configuración de ADC
	MOVLW	b'00001110'	;   Voltajes de Referencia con VCC (V+) y GND (V-) respectivamente
	MOVWF	ADCON1		;   Formato de resultado: Justificado a la izquierda
	BSF	STATUS,RP1
	MOVLW	b'00000011'
	MOVWF	ANSEL		;   RA0 y RA1 son entradas analógicas. El resto son entradas/salidas digitales
	BCF	STATUS,RP0
	BCF	STATUS,RP1
	BANKSEL	ANSELH
	CLRF	ANSELH
	BANKSEL	TMR0
	MOVLW	d'217'		;   Carga del TMR0 = 217. Se producirá el Overflow cada 5ms.
	MOVWF	TMR0
	MOVLW	0X01
	MOVWF	SELDISP		;   Seleccionamos el primer Display
	MOVWF	PORTC		;   Mostramos por PORTC
	CLRF	PORTC		;   Limpiamos PORTC
	CLRF	PORTE		;   Limpiamos PORTE
	MOVLW	0X00		;   Cargamos 0 en los setters de Unidad y Decena de Minutos y Horas
	MOVWF	SUMIN
	MOVWF	SDMIN
	MOVWF	SUHORA
	MOVWF	SDHORA
	MOVLW	d'3'	   
	MOVWF	CONT2
	MOVLW	0X04
	MOVWF	CONT
	MOVLW	0X04
	MOVWF	MODO
	MOVLW	d'200'
	MOVWF	CONT3
	CLRF	CMOD
	BCF	PORTA,5
	CLRF	CHAR0
	MOVLW	0X01
	MOVWF	ECONT
	MOVLW	d'3'	    ;	Se enviara por puerto serie cada 
	MOVWF	CONTCS	    ;

MAIN	CALL	BARRIDO	    ;	Llamamos a barrido y pushed para el teclado matricial
	CALL	PUSHED
	CALL	ADC	    ;	Llamamos al ADC
	MOVF	CMOD,W	    ;	Movemos CMOD a W
	MOVWF	PORTE	    ;	Cargamos PORTE con W
	BCF	STATUS,C    ;	Limpiamos el carry
	MOVF	AUX2,W	    ;	Cargamos W con AUX2 (Valor de Tension)
	SUBLW	d'60'	    ;	Le restamos a AUX2 60
	BTFSS	STATUS,C    ;	Comprobamos que el carry sea 0
	GOTO	CORTAR	    ;	Si es.  Vamos a cortar
	GOTO	NOCORT	    ;	No es.	Vamos a no cortar
	GOTO	MAIN	    ;	Volvemos al MAIN
	
CORTAR	BCF	PORTA,5	    ;	Seteamos en 1 el 5to bit de PORTA
	GOTO	MAIN	    ;	Volvemos al MAIN
	    
NOCORT	BSF	PORTA,5	    ;	Seteamos en 0 el 5to bit de PORTA
	GOTO	MAIN	    ;	Volvemos al MAIN
	
TMODO	BTFSC	MODO,0	    ;	Funcion para elegir el modo
	GOTO	SETHORA	    ;	Modo 0, Setea Hora
	BTFSC	MODO,1
	GOTO	VERHORA	    ;	Modo 1,	Visualiza Hora
	BTFSC	MODO,2	
	GOTO	SELDIGI	    ;	Modo 2,	ADC Voltaje
	BTFSC	MODO,3
	GOTO	SELDIGI	    ;	Modo 3,	ADC Corriente
	
BARRIDO	MOVF	CONT,W	    ;	Movemos CONT a W
	CALL	TFILAS	    ;	Llamamos a la tabla TFILAS
	MOVWF	PORTB	    ;	Lo que nos devuelve TFILAS  lo  mostramos en  PORTB
	RETURN		    ;	Volvemos
	
TFILAS	ADDWF	PCL,F	    
	RETLW	0X00	    ;	Devuelve 0
	RETLW	0X01	    ;	Devuelve 1
	RETLW	0X02	    ;	Devuelve 2
	RETLW	0X04	    ;	Devuelve 4
	RETLW	0X08	    ;	Devuelve 8
	
SETHORA	BTFSC	SELDISP,0   ;	De acuerdo al Display habilitado, setearemos la hora
	GOTO	SDUMIN	    ;	Vamos a setear la unidad de minutos
	BTFSC	SELDISP,1   
	GOTO	SDDMIN	    ;	Vamos a setear la decena de minutos
	BTFSC	SELDISP,2
	GOTO	SDUHORA	    ;	Vamos a setear la unidad de horas
	BTFSC	SELDISP,3
	GOTO	SDDHORA	    ;	Vamos a setear la decena de horas
	
SDUMIN	MOVF	SUMIN,W	    ;	Movemos el setter de unidad de minutos a W  
	MOVWF	NUMD	    ;	Carga W con NUMD
	GOTO	MOSTRAR	    ;	Llamamos a mostrar, con NUMD
	
SDDMIN	MOVF	SDMIN,W	    ;	Movemos el setter de decena de minutos a W 
	MOVWF	NUMD	    ;	Carga W con NUMD
	GOTO	MOSTRAR	    ;	Llamamos a mostrar, con NUMD
	
SDUHORA	MOVF	SUHORA,W    ;	Movemos el setter de unidad de horas a W 
	MOVWF	NUMD	    ;	Carga W con NUMD
	GOTO	MOSTRAR	    ;	Llamamos a mostrar, con NUMD
	
SDDHORA	MOVF	SDHORA,W    ;	Movemos el setter de decena de minutos a W 
	MOVWF	NUMD	    ;	Carga W con NUMD
	GOTO	MOSTRAR	    ;	Llamamos a mostrar, con NUMD
	
VERHORA	BTFSC	SELDISP,0   ;	Funcion para ver la hora de acuerdo al  Display habilitado
	GOTO	DUMIN	    ;	Si es el primer display, llama a DUMIN  
	BTFSC	SELDISP,1
	GOTO	DDMIN	    ;	Si es el segundo display, llama a DDMIN  
	BTFSC	SELDISP,2
	GOTO	DUHORA	    ;	Si es el tercer display, llama a DUHORA  
	BTFSC	SELDISP,3
	GOTO	DDHORA	    ;	Si es el cuarto display, llama a DDHORA  
	
DUMIN	MOVF	UMIN,W	    ;	Mueve UMIN a W	
	MOVWF	NUMD	    ;	Carga w con NUMD
	GOTO	MOSTRAR	    ;	Vamos a MOSTRAR
	
DDMIN	MOVF	DMIN,W	    ;	Mueve DMIN a W
	MOVWF	NUMD	    ;	Carga w con NUMD
	GOTO	MOSTRAR	    ;	Vamos a MOSTRAR
	
DUHORA	MOVF	UHORA,W	    ;	Mueve UHORA a W
	MOVWF	NUMD	    ;	Carga w con NUMD
	GOTO	MOSTRAR	    ;	Vamos a MOSTRAR
	
DDHORA	MOVF	DHORA,W	    ;	Mueve DHORA a W
	MOVWF	NUMD	    ;	Carga w con NUMD
	GOTO	MOSTRAR	    ;	Vamos a MOSTRAR
	
	;TECLADO

TC1	DECF	CONT,W	    ;	Decretamos CONT y lo guardamos en W
	CALL	TTC1	    ;	Llamamos a TTC1
BC1	BTFSC	PORTB,4	    ;	Comprobamos el 4to bit del PROTB 
	GOTO	BC1	    ;	Es 1, Vamos a BC1
	MOVWF	AUX	    ;	Es 0, cargamos W con AUX
	CALL	MODIFY	    ;	Llamamamos a MODIFY
	RETURN
	
TTC1	ADDWF	PCL,F	    ;	Tabla
	RETLW	D'1'	    ;	Devuelve 1
	RETLW	D'4'	    ;	Devuelve 4
	RETLW	D'7'	    ;	Devuelve 7
	RETLW	0X0A	    ;	Devuelve un salto de linea
	
TC2	DECF	CONT,W	    ;	Decretamos CONT y lo guardamos en W
	CALL	TTC2	    ;	Llamamos a TTC2
BC2	BTFSC	PORTB,5	    ;	Comprobamos el 5to bit del PROTB 
	GOTO	BC2	    ;	Es 1, Vamos a BC2
	MOVWF	AUX	    ;	Es 0, cargamos W con AUX
	CALL	MODIFY	    ;	Llamamamos a MODIFY
	RETURN
	
TTC2	ADDWF	PCL,f	    ;	Tabla
	RETLW	D'2'	    ;	Devuelve 2
	RETLW	D'5'	    ;	Devuelve 5
	RETLW	D'8'	    ;	Devuelve 8
	RETLW	D'0'	    ;	Devuelve 0

TC3	DECF	CONT,W	    ;	Decretamos CONT y lo guardamos en W
	CALL	TTC3	    ;	Llamamos a TTC3
BC3	BTFSC	PORTB,6	    ;	Comprobamos el 6to bit del PROTB 
	GOTO	BC3	    ;	Es 1, Vamos a BC3
	MOVWF	AUX	    ;	Es 0, cargamos W con AUX
	CALL	MODIFY	    ;	Llamamamos a MODIFY
	RETURN
	
TTC3	ADDWF	PCL,f	    ;	Tabla	  
	RETLW	D'3'	    ;	Devuelve 3
	RETLW	D'6'	    ;	Devuelve 6
	RETLW	D'9'	    ;	Devuelve 9
	RETLW	0X0B	    ;	Devuelve 
	
	;Para visualizar ADC
	
SELDIGI	BTFSC	SELDISP,0   ;	Funcion para seleccionar el digito usado
	GOTO	TUNI	    ;	Primero
	BTFSC	SELDISP,1
	GOTO	TDEC	    ;	Segundo
	BTFSC	SELDISP,2
	GOTO	TCEN	    ;	Tercero
	BTFSC	SELDISP,3
	GOTO	TE	    ;	Cuarto
	
TUNI	MOVF	NUM1,W	    ;	Si es el primer digito, movemos NUM1 a W
	MOVWF	NUMD	    ;	Cargamos NUMD con W
	GOTO	MOSTRAR	    ;	Vamos a mostrar
	
TDEC	MOVF	NUM2,W	    ;	Si es el segundi digito, movemos NUM2 a W
	MOVWF	NUMD	    ;	Cargamos NUMD con W
	GOTO	MOSTRAR	    ;	Vamos a mostrar
	
TCEN	MOVF	NUM3,W	    ;	Si es el tercer digito, movemos NUM3 a W
	MOVWF	NUMD	    ;	Cargamos NUMD con W
	GOTO	MOSTRAR	    ;	Vamos a mostrar
	
TE	MOVLW	0X0E	    ;	Si es el cuarto digito, movemos "E" a W
	MOVWF	NUMD	    ;	Cargamos NUMD con W
	GOTO	MOSTRAR	    ;	Vamos a mostrar
	
DISPLAY	ADDWF	PCL	    ;	Tabla para display 7 segmentos catodo comun a=lsb
.0	RETLW	0X3F	    ;	b'00111111';0
.1	RETLW	0X06	    ;	b'00000110';1
.2	RETLW	0X5B	    ;	b'01011011';2
.3	RETLW	0X4F	    ;	b'01001111';3
.4	RETLW	0X66	    ;	b'01100110';4
.5	RETLW	0X6D	    ;	b'01101101';5
.6	RETLW	0X7D	    ;	b'01111101';6
.8	RETLW	0X07	    ;	b'00000111';7
.7	RETLW	0X7F	    ;	b'01111111';8
.9	RETLW	0X67	    ;	b'01100111';9
.A	RETLW	0X00
.B	RETLW	0X00
.C	RETLW	b'00111001'
.D	RETLW	0X00
.E	RETLW	b'01111001'
.F	RETLW	0X00
	
INTER	MOVWF	GW	    ;	Rutina de interrupcion
	SWAPF	STATUS,W    ;	Guardamos los registros W y STATUS
	MOVWF	GSTAT
	BTFSC	INTCON,2    ;	Configuramos el registro INTCON para el timmer
	GOTO	TIMMER	    ;	Vamos  a timmer
VOLVER	MOVLW	b'10100000' ;	Configuramos el registro INTCON
	MOVWF	INTCON	
	SWAPF	GSTAT,W	    ;	Recuperamos los registros W y STATUS
	MOVWF	STATUS
	MOVF	GW,W
	RETFIE
	
TIMMER	DECFSZ	CONTCS,F    ;	Decrementramos CONTCS y lo guardamos en si mismo, cuando llega a 0
	GOTO	NOENV	    ;	Es 1, vamos a NOENV
	CALL	ENVIAR	    ;	Llego a 0, vamos  a ENVIAR
NOENV	BCF	INTCON,2    ;	Configuramos el registro INTCON
	BTFSC	MODO,1	    ;	Vemos si esta en modo 1
	DECFSZ	CONT3	    ;	Si esta, decrementamos CONT3 hasta que sea cero y saltamos a llamar a UNSEG
	GOTO	SEGUIR	    ;	No esta,  vamos a SEGUIR
	CALL	UNSEG	    ;	Llama a UNSEG
SEGUIR	BCF	STATUS,C    ;	Seteamos el carry en 0
	RLF	SELDISP,F   ;	Rotamos SELDISP para recorrerlos
	BTFSC	SELDISP,4   ;	Comprobamos si llego a pasar todos los display
	CALL	RESETSD	    ;	Si, llamamos a RESETSD
	MOVLW	d'217'	    ;	No, cargamos el timmer nuevamente
	MOVWF	TMR0
	GOTO	TMODO	    ;	Volvemos a TMODO
MOSTRAR	MOVF	NUMD,W	    ;	Funcion para mostrar por display. Movemos NUMD a W		    
	CALL	DISPLAY	    ;	Llamamos a la tabla DISPLAY
	MOVWF	PORTD	    ;	Cargamos PORTD con lo que nos devolvio DISPLAY
	MOVF	SELDISP,W   ;	Cargamos W con el display seleccionado
	MOVWF	PORTC	    ;	Cargamos PORTC con el display seleccionado
	DECFSZ	CONT2,F	    ;	Decrementamos CONT2 hasta que sea 0 y lo guardamos en si mismo
	GOTO	VOLVER	    ;	Hasta que sea 0 vamos a VOLVER
	CALL	DECCONT	    ;	Es cero, llamamos a DECCONT
	GOTO	VOLVER	    ;	Vamos a VOLVER
	
ENVIAR	CALL	ADC	    ;	Funcion para mostrar por serie los datos.  Llamamos a ADC, ACTCH y TDATO
	CALL	ACTCH
	CALL	TDATO
	BSF	STATUS,RP0
	MOVLW	D'25'	    ;	Baud rate = 9600bps
	MOVWF	SPBRG	    ;	A 4MHz
	MOVLW	B'00100100' 
	MOVWF	TXSTA	    ;	Configura TXSTA con transmision de 8 bits, habilita la transmision, modo asincrono, alta velocidad
	BCF	STATUS, RP0 ;	Banco 0
	MOVLW	B'10000000'
	MOVWF	RCSTA	    ;	Habilita los puertos seriales
	BCF	STATUS,RP0
	MOVF	CHAR0, W    ;	Cargo W con CHAR0
	MOVWF	TXREG	    ;	Movemos W al registro TXREG, que es el que muestra(envia) en serie
	BSF	STATUS, RP0 ;	Banco 1
WTHERE	BTFSS	TXSTA, TRMT ;	TRMT esta vacio?
	GOTO	WTHERE	    ;	No, vuelvo a comprobar
	CLRF	TXSTA	    ;	Limpiamos TXSTA
	BCF	STATUS, RP0 
	BCF	STATUS,C    ;	Seteamos a 0 el  Carry
	RLF	ECONT,F	    ;	Rotamos ECONT y lo guardamos en si mismo
	BTFSC	ECONT,5	    ;	Comprobamos el estado del 5to bit de ECONT
	CALL	RESEC	    ;	Es 1, llamamos a RESEC
	MOVLW	d'3'	    ;	Es 0, cargamos W con 3
	MOVWF	CONTCS	    ;	Cargamos CONTCS con 3
	RETURN
	
DECCONT	MOVLW	d'3'	    ;	Cargamos W con 3
	MOVWF	CONT2	    ;	Cargamos CONT2 con 3
	DECFSZ	CONT,F	    ;	Decrementamos CONT y lo  guardamos en si mismo, hasta que sea cero
	RETURN		    ;	Es 1, volvemos
	MOVLW	0X04	    ;	Es 0, cargamos w con 4
	MOVWF	CONT	    ;	Cargamos CONT con 4
	RETURN
	
UNSEG	BCF	STATUS,Z    ;	Seteamos en 0 el bit Z de STATUS para realizar la  siguiente operacion de resta
	MOVLW	d'200'	    ;	Cargamos  W con 200
	MOVWF	CONT3	    ;	Cargamos CONT3 con 200
	INCF	SEG,F	    ;	Incrementamos SEG y lo guardamos en si mismo
	MOVLW	d'60'	    ;	Cargamos w  con 60
	SUBWF	SEG,W	    ;	Restamos SEG a W
	BTFSC	STATUS,Z    ;	Comprobamos si da cero
	CALL	RESSEG	    ;	Da 0, llamamos a RESSEG
	RETURN		    ;	Todavia no da 0, volvemos
	
ACTCH	MOVF	UNIDAD,W    ;	Movemos UNIDAD a W
	ADDLW	0X30	    ;	Sumamos 0x30 a W
	MOVWF	CHARU	    ;	Movemos ese valor a CHARU  (Por ascii)
	MOVF	DECENA,W    ;	Movemos DECENA a W
	ADDLW	0X30	    ;	Sumamos 0x30 a W
	MOVWF	CHARD	    ;	Movemos ese valor a CHARD  (Por ascii)
	MOVF	CENTENA,W   ;	Movemos CENTENA a W
	ADDLW	0X30	    ;	Sumamos 0x30 a W
	MOVWF	CHARC	    ;	Movemos ese valor a CHARC  (Por ascii)
	RETURN
	
RESEC	MOVLW	0X01	   
	MOVWF	ECONT
	RETURN
	
TDATO	BTFSC	ECONT,0	    ;	Funcion para ver que caracter se envia por  serie
	CALL	ENVUNI	    ;	Si ECONT,1 es 1, llamamos a ENVUNI (envia unidad)
	BTFSC	ECONT,1
	CALL	ENVCOM	    ;	Si ECONT,2 es 1, llamamos a ENVCOM (envia coma)
	BTFSC	ECONT,2
	CALL	ENVDEC	    ;	Si ECONT,3 es 1, llamamos a ENVDEC (envia decena)
	BTFSC	ECONT,3
	CALL	ENVCEN	    ;	Si ECONT,4 es 1, llamamos a ENVCEN (envia centena)
	BTFSC	ECONT,4
	CALL	ENVCR	    ;	Si ECONT,5 es 1, llamamos a ENVCR (envia retorno de carro)
	BTFSC	ECONT,5
	CALL	ENVSALT	    ;	Si ECONT,6 es 1, llamamos a ENVSALT (envia salto de linea)
	RETURN
	
ENVUNI	MOVF	CHARC,W	    ;	Mueve CHARC a W
	MOVWF	CHAR0	    ;	Carga CHAR0 con	W 
	RETURN

ENVCOM	MOVLW	0X2C	    ;	Mueve 0x2c(Coma) a W
	MOVWF	CHAR0	    ;	Carga CHAR0 con	W 
	RETURN
	
ENVDEC	MOVF	CHARD,W	    ;	Mueve CHARD a W
	MOVWF	CHAR0	    ;	Carga CHAR0 con	W 
	RETURN
	
ENVCEN	MOVF	CHARU,W	    ;	Mueve CHARU a W
	MOVWF	CHAR0	    ;	Carga CHAR0 con	W 
	RETURN
	
ENVCR	MOVLW	0X0D	    ;	Mueve 0x0d (retorno de carro) a W
	MOVWF	CHAR0	    ;	Carga CHAR0 con	W 
	RETURN
	
ENVSALT	MOVLW	0X0A	    ;	Mueve 0x0a (Salto de  linea) a W
	MOVWF	CHAR0	    ;	Carga CHAR0 con	W 
	RETURN
	
PUSHED	BTFSC	PORTB,4	    ;	Funcion para el teclado
	CALL	TC1	    ;	Si el 4to bit del PORTB es 1, llama a TC1
	BTFSC	PORTB,5
	CALL	TC2	    ;	Si el 5to bit del PORTB es 1, llama a TC1
	BTFSC	PORTB,6
	CALL	TC3	    ;	Si el 6to bit del PORTB es 1, llama a TC1
IRSE	RETURN	
	
MODIFY	BCF	STATUS,Z    ;	Setea el bit  Z de STATUS en 0
	MOVF	AUX,W	    ;	Mueve la  tecla apretada a W
	SUBLW	0X0A	    ;	Le restamos 0x0A (10) a W
	BTFSC	STATUS,Z    ;	Comprobamos si da 0
	GOTO	CHMOD	    ;	Es 0, vamos a CHMOD
	MOVF	AUX,W	    ;	Es 1, movemos la tecla apretada a W
	SUBLW	0X0B	    ;	Le restamos 0x0B (11) a W
	BTFSC	STATUS,Z    ;	Comprobamos si da 0
	GOTO	CHMOD	    ;	Es 0, vamos a CHMOD
	MOVF	SUHORA,W    ;	Es 1, movemos SUHORA a W
	MOVWF	SDHORA	    ;	Cargamos SDHORA con W
	MOVF	SDMIN,W	    ;	Movemos SDMIN a W
	MOVWF	SUHORA	    ;	Cargamos SUHORA con W
	MOVF	SUMIN,W	    ;	Movemos SUMIN a W
	MOVWF	SDMIN	    ;	Cargamos SDMIN
	MOVF	AUX,W
	MOVWF	SUMIN
SALT	RETURN

	
ADC	BCF	ADCON0,2	;   Seleccionamos el CH0
	BSF	ADCON0,GO_DONE	;   Emepzamos la conversion
BA1	BTFSC	ADCON0,GO_DONE	;   Comprobamos si ya termino la conversion
	GOTO	BA1		;   No, volvemos a BA1
	MOVF	ADRESH,W	;   Si, movemos ADRESH a W
	MOVWF	AUX3		;   Cargamos  AUX3 con W
	BCF	AUX3,2		;   Seteamos los bits 0 y 2 de AUX3 en 0, para manejar un rango de valores de 0 a 250 del valor leido del ADC
	BCF	AUX3,0		;   para tener un incremento exacto de 0,02V
	BSF	ADCON0,2	;   Seleccionamos el CH1
	BSF	ADCON0,GO_DONE	;   Emepzamos la conversion
BA	BTFSC	ADCON0,GO_DONE	;   Comprobamos si ya termino la conversion
	GOTO	BA		;   No, volvemos a BA
	MOVF	ADRESH,W	;   Si, movemos ADRESH a W
	MOVWF	AUX2		;   Cargamos  AUX2 con W
	BCF	AUX2,2		;   Seteamos los bits 0 y 2 de AUX3 en 0, para manejar un rango de valores de 0 a 250 del valor leido del ADC
	BCF	AUX2,0		;   para tener un incremento exacto de 0,02A
	MOVF	AUX2,W		;   Movemos AUX2 a W
	CLRF	UNIDAD		;   Limpiamos UNIDAD
	CLRF	DECENA		;   Limpiamos DECENA
	CLRF	CENTENA		;   Limipiamos CENTENA
	BTFSC	MODO,3		;   Comprobamos si estamos en el Modo 3
	MOVF	AUX3,W		;   Si, movemos AUX3 a  W
	BTFSC	MODO,2		;   No, comprobamos si estamos en el Modo 2
	MOVF	AUX2,W		;   Si, movemos AUX2 a W
	MOVWF	MCONT		;   No, cargamos MCONT con W
ACTN	MOVLW	0X02		;   Cargamos W con 0x02
	ADDWF	UNIDAD,F	;   Sumamos UNIDAD a W y lo guardamos en UNIDAD
	BCF	STATUS,Z	;   Ponemos el  bit Z en 0
	MOVF	UNIDAD,W	;   Movemos UNIDAD a W
	SUBLW	0X0A		;   Le restamos el literal 0x0A
	BTFSC	STATUS,Z	;   Comprobamos si es da 0 esa resta
	CALL	SOBREUN		;   Si,	llama a SOBREUN
	BCF	STATUS,Z	;   No, setea en 0 el bit Z
	DECFSZ	MCONT,F		;   Decrementamos MCONT y lo guardamos sobre si mismo hasta que sea 0
	GOTO	ACTN		;   Si no es 0,  vamos a ACTN
	MOVF	UNIDAD,W	;   Si es 0, movemos UNIDAD a W
	MOVWF	NUM1		;   Cargamos NUM1 con W
	MOVF	DECENA,W	;   Movemos DECENA a W
	MOVWF	NUM2		;   Cargamos NUM2 con W
	MOVF	CENTENA,W	;   Movemos CENTENA a W
	MOVWF	NUM3		;   Cargamos NUM3 con W. Todos estos son para correr el dato hacia la izquierda
	RETURN
	
SOBREUN	CLRF	UNIDAD		;   Limpiamos UNIDAD
	INCF	DECENA,F	;   Incrementamos DECENA en uno y lo guardamos  sobre si  mismo
	BCF	STATUS,Z	;   Seteamos el  bit Z en 0 
	MOVF	DECENA,W	;   Movemos DECENA a W
	SUBLW	0X0A		;   Le  restamos 0x0A para que no exceda de 9
	BTFSC	STATUS,Z	;   Comprobamos si la resta da 0
	CALL	SODEC		;   Si da 0, llamamos a SODEC
	RETURN			;   Si no da 0, retornamos 
	
SODEC	BCF	STATUS,Z	;   Seteamos el bit  Z en 0
	CLRF	DECENA		;   Limpiamos DECENA
	INCF	CENTENA,F	;   Incrementamos CENTENA en uno y lo guradamos sobre si mismo
	RETURN	
	
RESETSD	MOVLW	0X01		;   Reseteador. Cargamos W con el literal 0X01
	MOVWF	SELDISP		;   Cargamos SELDISP con W
	RETURN
	
CHMOD	CALL	HSET		;   Llamamos a HSET
	BCF	STATUS,C	;   Seteamos el  carry en 0
	RLF	MODO,F		;   Rotamos el modo y lo guardamos sobre si mismo
	BTFSC	MODO,4		;   Comprobamos si el bit 4 de  MODO esta en 1, es  decir que se  paso de los  modos  disponibles
	CALL	RESMODO		;   Si es asi llama a RESMODO
	INCF	CMOD,F		;   Si no, incrementa  CMOD  y lo guarda sobre si mismo
	MOVLW	0X04		;   Cargamos  W  con el literal 0X04
	SUBWF	CMOD,W		;   Le restamos a W CMOD
	BTFSC	STATUS,Z	;   Comprobamos que la resta de 0
	CALL	RESMODO		;   Si da 0, llamamos a RESMODO
	GOTO	SALT		;   Si no da 0, vamos a SALT
	
RESMODO	MOVLW	0X01		;   Reseteador. Cargamos W  con  el  literal 0X01
	MOVWF	MODO		;   Cargamos MODO con W
	CLRF	CMOD		;   Limpiamos CMOD
	RETURN
	    
HSET	MOVF	SUMIN,W		;   Movemos SUMIN a W
	MOVWF	UMIN		;   Cargamos UMIN con W
	MOVF	SDMIN,W		;   Movemos SDMIN a W
	MOVWF	DMIN		;   Cargamos DMIN con W
	MOVF	SUHORA,W	;   Movemos  SUHORA a W
	MOVWF	UHORA		;   Cargamos UHORA con w
	MOVF	SDHORA,W	;   Movemos SDHORA a W
	MOVWF	DHORA		;   Cargamos DHORA con W
	RETURN
	
RESSEG	BCF	STATUS,Z	;   Seteamos el bit Z en 0
	CLRF	SEG		;   Limpiamos SEG
	INCF	UMIN,F		;   Incrementamos UMIN en uno y lo guardamos sobre si mismo
	MOVLW	0X0A		;   Cargamos W con el literal 0X0A (10, para que no pase de 9)
	SUBWF	UMIN,W		;   Le restamos UMIN
	BTFSC	STATUS,Z	;   Comprobamos que la resta de 0
	CALL	RESUMIN		;   Si da 0,  llamamos a RESUMIN
	RETURN			;   Si no da 0, retornamos
	
RESUMIN	CLRF	UMIN		;   Limpiamos UMIN
	INCF	DMIN,F		;   Incrementamos DMIN en uno y lo guardamos sobre si mismo
	MOVLW	0X06		;   Cargamos W con el literal 0X06 (6, para que no pase de 5)
	SUBWF	DMIN,W		;   Le restamos DMIN
	BTFSC	STATUS,Z	;   Comprobamos que la resta de 0
	CALL	RESDMIN		;   Si da 0,  llamamos a RESDMIN
	RETURN			;   Si no da 0, retornamos
	
RESDMIN	CLRF	DMIN		;   Limpiamos DMIN
	INCF	UHORA,F		;   Incrementamos UHORA en uno y lo guardamos sobre si mismo
	MOVLW	0X0A		;   Cargamos W con el literal 0X0A (10, para que no pase de 9)
	SUBWF	UHORA,W		;   Le restamos UHORA
	BTFSC	STATUS,Z	;   Comprobamos que la resta de 0
	CALL	RESUH		;   Si da 0,  llamamos a RESUH
	RETURN			;   Si no da 0, retornamos
	
RESUH	CLRF	UHORA		;   Limpiamos UHORA
	INCF	DHORA		;   Incrementamos DHORA en uno y lo guardamos sobre si mismo
	MOVLW	0X06		;   Cargamos W con el literal 0X06 (6, para que no pase de 5)
	SUBWF	DHORA,W		;   Le restamos DHORA
	BTFSC	STATUS,Z	;   Comprobamos que la resta de 0
	CALL	RESDH		;   Si da 0,  llamamos a RESDH
	RETURN			;   Si no da 0, retornamos
	
RESDH	CLRF	DHORA		;   Limpiamos DHORA
	RETURN
		

    END